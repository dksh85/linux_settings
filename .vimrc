set autoindent
set cindent
set smartindent
set number
set shiftwidth=3
set tabstop=3
set hlsearch
set wrap
set background=light
syntax on
set bs=2
